const mainColors = {
    blue1: '#3D96CA',
    blue2: '#EDFCFD',
    blue3: '#8BDCEC',
    blue4: '#0BCAD4',
    dark0: '#15191F',
    dark1: '#252D35',
    dark2: '#495A75',
    grey1: '#7D8797',
    grey2: '#E9E9E9',
    grey3: '#495A75',
    grey4: '#B1B7C2',
    black1: '#000000',
    black2: 'rgba(0, 0, 0, 0.5)',
    red1: '#E06379',
}

export const colors = {
    primary: mainColors.blue1,
    secondary: mainColors.dark1,
    white: 'white',
    black: 'black',
    dark: mainColors.dark0,
    disable: mainColors.grey3,
    tertiary: mainColors.blue4,
    text: {
        primary: 'white',
        secondary: mainColors.grey1,
        menuInactive: mainColors.dark2,
        menuActive: mainColors.blue1,
        chat: 'black',
        statopen: mainColors.blue1,
        statclose: mainColors.red1,
    },
    statuscard: mainColors.blue3,
    button: {
        primary: {
            bacground: mainColors.blue1,
            text: 'white'
        },
        secondary: {
            background: 'white',
            text: mainColors.dark1,
        },
        disable: {
            bacground: mainColors.grey2,
            text: mainColors.grey4,
        },
    },
    border: mainColors.grey2,
    cardLight: mainColors.blue2,
    loadingBackground: mainColors.black2,
    error: mainColors.red1,
}