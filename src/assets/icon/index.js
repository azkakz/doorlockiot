import IconBackLight from './ic-back-light.svg';
import IconAddPhoto from './ic-add-photo.svg';
import IconRemovePhoto from './ic-remove-photo.svg';
import IconLock from './ic-lock.svg'
import IconLockActive from './ic-lock-active.svg'
import IconMessages from './ic-messages.svg'
import IconMessagesActive from './ic-messages-active.svg'
import IconStatistics from './ic-statistics.svg'
import IconStatisticsActive from './ic-statistics-active.svg'
import IconLockStatus from './ic-lockstatus.svg'
import IconLockStatusActive from './ic-lockstatus-active.svg'
import IconSendDark from './ic-send-dark.svg'
import IconSendLight from './ic-send-light.svg'
import IconEditProfile from './ic-edit-profile.svg'
import IconLanguage from './ic-language.svg'
import IconRate from './ic-rate.svg'
import IconHelp from './ic-help.svg'
import IconLogOut from './ic-log-out.svg'

export { IconBackLight, IconAddPhoto, IconRemovePhoto, IconLock, IconLockActive, IconMessages, IconMessagesActive, IconStatistics, IconStatisticsActive, IconLockStatus, IconLockStatusActive, IconSendDark, IconSendLight, IconEditProfile, IconLanguage, IconRate, IconHelp, IconLogOut }