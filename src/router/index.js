import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { GetStarted, Login, Register, Splash, UploadPhoto, Lock, Messages, Statistics, Chatting, UserProfile, UpdateProfile, MessagesAdmin, ChattingAdmin, StatisticsAdmin } from "../pages";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { BottomNavigator } from "../components";
import AdminHome from "../pages/AdminHome";

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen name="Lock" component={Lock} options={{ headerShown: false }} />
            <Tab.Screen name="Messages" component={Messages} options={{ headerShown: false }} />
            <Tab.Screen name="Statistics" component={Statistics} options={{ headerShown: false }} />
        </Tab.Navigator>
    )
}

const Router = () => {
    return (
        <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
            <Stack.Screen name="GetStarted" component={GetStarted} options={{ headerShown: false }} />
            <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="UploadPhoto" component={UploadPhoto} options={{ headerShown: false }} />
            <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }} />
            <Stack.Screen name="Chatting" component={Chatting} options={{ headerShown: false }} />
            <Stack.Screen name="UserProfile" component={UserProfile} options={{ headerShown: false }} />
            <Stack.Screen name="UpdateProfile" component={UpdateProfile} options={{ headerShown: false }} />
            <Stack.Screen name="AdminHome" component={AdminHome} options={{ headerShown: false }} />
            <Stack.Screen name="MessagesAdmin" component={MessagesAdmin} options={{ headerShown: false }} />
            <Stack.Screen name="ChattingAdmin" component={ChattingAdmin} options={{ headerShown: false }} />
            <Stack.Screen name="StatisticsAdmin" component={StatisticsAdmin} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
};

export default Router;