import React from 'react'
import { TouchableOpacity } from 'react-native'
import { IconBackLight } from '../../../assets'

const IconOnly = ({ onPress, icon }) => {
    const Icon = () => {
        if (icon === 'back-light') {
            return <IconBackLight />
        }
        if (icon === 'back-light') {
            return <IconBackLight />
        }
        return <IconBackLight />
    }
    return (
        <TouchableOpacity onPress={onPress}>
            <Icon />
        </TouchableOpacity>
    )
}

export default IconOnly
