import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconLock, IconLockActive, IconMessages, IconMessagesActive, IconStatistics, IconStatisticsActive } from '../../../assets'
import { colors, fonts } from '../../../utils'

const TabItem = ({ title, active, onPress, onLongPress }) => {
    const Icon = () => {
        if (title === 'Lock') {
            return active ? <IconLockActive /> : <IconLock />
        }
        if (title === 'Messages') {
            return active ? <IconMessagesActive /> : <IconMessages />
        }
        if (title === 'Statistics') {
            return active ? <IconStatisticsActive /> : <IconStatistics />
        }
        return <IconLock />
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
            <Icon />
            <Text style={styles.text(active)}>{title}</Text>
        </TouchableOpacity>
    )
}

export default TabItem

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    text: (active) => ({
        fontSize: 10,
        color: active ? colors.text.menuActive : colors.text.menuInactive,
        fontFamily: fonts.primary[600],
        marginTop: 4,
    }),
})
