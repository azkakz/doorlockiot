import React, { useEffect, useState } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { DummyUser, ILNullPhoto } from '../../../assets'
import { colors, fonts, getData } from '../../../utils'
import { getDatabase, ref, onValue } from "firebase/database";

const HomeProfile = ({ onPress }) => {
    const [profile, setProfile] = useState({
        photo: ILNullPhoto,
        fullName: '',
        ruangan: '',
    })
    const [status, setStatus] = useState('')

    useEffect(() => {
        getData('user').then(res => {
            const data = res
            data.photo = { uri: res.photo }
            setProfile(data)

            const db = getDatabase();
            return onValue(ref(db, `users/${data.uid}/`), (snapshot) => {
                const datalive = (snapshot.val()) || 'Gagal memuat';
                setStatus(datalive.status ? 'Verified' : 'Not Verified')
            }, {
                onlyOnce: false
            });
        })
    }, [])

    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Image source={profile.photo} style={styles.avatar} />
            <View>
                <Text style={styles.name}>{profile.fullName}</Text>
                <Text style={styles.ruangan}>Ruangan {profile.ruangan} ({status})</Text>
            </View>
        </TouchableOpacity>
    )
}

export default HomeProfile

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
        marginRight: 12,
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        textTransform: 'capitalize',
    },
    ruangan: {
        fontSize: 14,
        fontFamily: fonts.primary[400],
        color: colors.text.secondary,
        textTransform: 'capitalize',
    },
    // status: {
    //     fontSize: 14,
    //     fontFamily: fonts.primary[400],
    //     color: colors.text.secondary,
    //     textTransform: 'capitalize',
    // }
})
