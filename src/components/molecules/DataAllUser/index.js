import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconEditProfile, IconHelp, IconLanguage, IconRate } from '../../../assets'
import { colors, fonts } from '../../../utils'

const List = ({ profile, name, ruangan, status, onPress, icon, onPressAccess }) => {
    const Icon = () => {
        if (icon === 'edit-profile') {
            return <IconEditProfile />
        }
        if (icon === 'language') {
            return <IconLanguage />
        }
        if (icon === 'rate') {
            return <IconRate />
        }
        if (icon === 'help') {
            return <IconHelp />
        }
        return <IconEditProfile />
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            {icon ? <Icon /> : <Image source={profile} style={styles.avatar} />}
            <View style={styles.content}>
                <Text style={styles.name}>{name}</Text>
                <Text style={styles.ruangan}>Ruangan: {ruangan}</Text>
                <Text style={styles.status}>{status ? 'Verified' : 'Not Verified'}</Text>
            </View>
            <TouchableOpacity style={styles.containerAccess} onPress={onPressAccess}>
                <Text style={styles.textButtonAccess}>Add/Remove Access</Text>
            </TouchableOpacity>
        </TouchableOpacity>
    )
}

export default List

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        alignItems: 'center',
    },
    content: {
        flex: 1,
        marginLeft: 16,
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
    },
    name: {
        fontSize: 16,
        fontFamily: fonts.primary.normal,
        color: colors.text.primary,
    },
    ruangan: {
        fontSize: 13,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
    },
    status: {
        fontSize: 13,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary,
    },
    containerAccess: {
        maxWidth: 100,
        alignItems: 'center',
    },
    textButtonAccess: {
        fontSize: 13,
        fontFamily: fonts.primary[300],
        color: colors.text.primary,
        textAlign: 'center'
    }
})
