import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { Button } from '../..'
import { DummyUser } from '../../../assets'
import { colors, fonts } from '../../../utils'

const Profile = ({ onPress, title, desc, photo }) => {
    return (
        <View style={styles.container}>
            <Button type="icon-only" icon="back-light" onPress={onPress} />
            <View style={styles.contet}>
                <Text style={styles.name}>{title}</Text>
                <Text style={styles.desc}>Ruangan {desc}</Text>
            </View>
            <Image source={photo} style={styles.avatar} />
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.secondary,
        paddingVertical: 30,
        paddingLeft: 20,
        paddingRight: 16,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    contet: {
        flex: 1,
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
    },
    name: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        textAlign: 'center',
    },
    desc: {
        fontSize: 14,
        fontFamily: fonts.primary.normal,
        color: colors.text.secondary,
        marginTop: 6,
        textAlign: 'center',
    },
})
