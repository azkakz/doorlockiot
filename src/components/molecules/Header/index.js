import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button, Gap } from '../../atoms'
import { colors, fonts } from '../../../utils'
import Profile from './Profile'

const Header = ({ onPress, title, type, desc, photo }) => {
    if (type === 'profile') {
        return <Profile onPress={onPress} />
    }
    if (type === 'admin-profile') {
        return <Profile onPress={onPress} title={title} desc={desc} photo={photo} />
    }
    if (type === 'user-profile') {
        return <Profile onPress={onPress} title={title} desc={desc} photo={photo} />
    }

    return (
        <View style={styles.container}>
            <Button type="icon-only" icon="back-light" onPress={onPress} />
            <Text style={styles.text}>{title}</Text>
            <Gap width={24} />
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 16,
        paddingVertical: 30,
        backgroundColor: colors.dark,
        flexDirection: 'row',
        alignItems: 'center',
    },
    text: {
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
    }
})
