import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { IconLockStatus, IconLockStatusActive } from '../../../assets'
import { colors } from '../../../utils'

const LockStatus = ({ onPress, isLock }) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            {isLock === 'Locked' && (
                <IconLockStatusActive />
            )}
            {isLock === 'Unlocked' && (
                <IconLockStatus />
            )}
        </TouchableOpacity>
    )
}

export default LockStatus

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.statuscard,
        width: 250,
        height: 325,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
