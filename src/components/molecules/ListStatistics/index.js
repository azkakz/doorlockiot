import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors, fonts } from '../../../utils'

const ListStatistics = ({ timeOpen, timeClose, statcolor }) => {
    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Text style={styles.time(statcolor)}>{timeOpen}</Text>
            </View>
        </View>
    )
}

export default ListStatistics

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 6,
        // borderBottomWidth: 1,
        // borderBottomColor: colors.border,
        alignItems: 'center',
    },
    content: {
        flex: 1,
        marginLeft: 16,
    },
    time: (statcolor) => ({
        fontSize: 14,
        fontFamily: fonts.primary[300],
        color: statcolor === 'buka' ? colors.text.statopen : colors.text.statclose,
    }),
})
