import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { DummyUser } from '../../assets'
import { List } from '../../components'
import { colors, fonts, getData } from '../../utils'
import { Firebase } from '../../config'
import { getDatabase, ref, set, update, get, child, onValue } from "firebase/database";

const Messages = ({ navigation }) => {
    const [admins, setAdmins] = useState([])
    const [user, setUser] = useState({})
    const [historyChat, setHistoryChat] = useState([])

    useEffect(() => {
        getData('user').then(res => {
            setUser(res)
            const db = getDatabase();
            onValue(ref(db, `users/`), (snapshot) => {
                if (snapshot.val()) {
                    const oldData = snapshot.val()
                    const data = []
                    Object.keys(oldData).map(key => {
                        data.push({
                            id: key,
                            data: oldData[key]
                        })
                    })
                    setAdmins(data)
                }
            }, {
                onlyOnce: false
            });

            const urlHistory = `messages/${res.uid}/`
            return onValue(ref(db, urlHistory), (snapshot) => {
                if (snapshot.val()) {
                    console.log('Data history: ', snapshot.val())
                    const oldData = snapshot.val()
                    const data = []
                    Object.keys(oldData).map(key => {
                        data.push({
                            id: key,
                            ...oldData[key],
                        })
                    })
                    setHistoryChat(data)
                }
            }, {
                onlyOnce: false
            });
        })
    }, [])

    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <Text style={styles.title}>Messages</Text>
                {
                    admins.map(admin => {
                        if (admin.data.role === 'admin') {
                            return <List key={admin.id} profile={{ uri: admin.data.photo }} name={admin.data.fullName} desc={
                                historyChat.map(chat => {
                                    if (admin.data.uid === chat.uidPartner) {
                                        return chat.lastContentChat
                                    }
                                })
                            } onPress={() => navigation.navigate('Chatting', { admin })} />
                        }
                    })
                }
            </View>
        </View>
    )
}

export default Messages

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary,
        flex: 1,
    },
    content: {
        backgroundColor: colors.dark,
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    title: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginLeft: 16,
    },
})
