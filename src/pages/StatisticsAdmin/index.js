import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Gap, ListStatistics } from '../../components'
import { colors, fonts, getData } from '../../utils'
import { Firebase } from '../../config'
import { getDatabase, ref, set, update, get, child, onValue } from "firebase/database";

const Statistics = ({ navigation, route }) => {
    const dataUser = route.params
    const [historyOpen, setHistoryOpen] = useState([])
    const [historyClose, setHistoryClose] = useState([])
    const [historyKey] = ([historyOpen.concat(historyClose)])
    historyKey.sort(function (a, b) { return a.timestamp - b.timestamp });

    useEffect(() => {
        console.log('res di stat: ', dataUser.user.data.ruangan)
        const db = getDatabase();
        onValue(ref(db, `ruangan/${dataUser.user.data.ruangan}/waktuOpen`), (snapshot) => {
            if (snapshot.val()) {
                const oldData = snapshot.val()
                const data = []
                Object.keys(oldData).map(key => {
                    data.push({
                        id: key,
                        time: new Date(oldData[key] * 1000).toUTCString() + ' (Open)',
                        timestamp: oldData[key],
                        keterangan: 'buka'
                    })
                })
                setHistoryOpen(data)

                console.log('res di data stat: ', data)
            }
        }, {
            onlyOnce: false
        });

        onValue(ref(db, `ruangan/${dataUser.user.data.ruangan}/waktuClose`), (snapshot) => {
            if (snapshot.val()) {
                const oldData = snapshot.val()
                const data = []
                Object.keys(oldData).map(key => {
                    data.push({
                        id: key,
                        time: new Date(oldData[key] * 1000).toUTCString() + ' (Close)',
                        timestamp: oldData[key],
                        keterangan: 'tutup'
                    })
                })
                setHistoryClose(data)

                console.log('res di data stat: ', data)
            }
        }, {
            onlyOnce: false
        });
        console.log('data gabungan: ', historyKey)
    }, [])

    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <View style={styles.navigationAdmin}>
                    <Text style={styles.title}>Statistics</Text>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.homeButton}>Home</Text>
                    </TouchableOpacity>
                </View>
                <Gap height={15} />
                {
                    historyKey.map(historykey => {
                        return <ListStatistics key={historykey.id} timeOpen={historykey.time} statcolor={historykey.keterangan} />
                    })
                }
            </View>
        </View>
    )
}

export default Statistics

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary,
        flex: 1,
    },
    content: {
        backgroundColor: colors.dark,
        flex: 1,
    },
    navigationAdmin: {
        flexDirection: 'row',
        marginRight: 16,
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginLeft: 16,
    },
    homeButton: {
        fontSize: 13,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 10,
        marginLeft: 16,
    }
})
