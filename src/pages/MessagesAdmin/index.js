import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { DummyUser } from '../../assets'
import { List } from '../../components'
import { colors, fonts, getData } from '../../utils'
import { Firebase } from '../../config'
import { getDatabase, ref, set, update, get, child, onValue } from "firebase/database";

const MessagesAdmin = ({ navigation }) => {
    const [users, setUsers] = useState([])
    const [historyChat, setHistoryChat] = useState([])

    useEffect(() => {
        getData('user').then(res => {
            const db = getDatabase();
            onValue(ref(db, `users/`), (snapshot) => {
                if (snapshot.val()) {
                    const oldData = snapshot.val()
                    const data = []
                    Object.keys(oldData).map(key => {
                        data.push({
                            id: key,
                            data: oldData[key]
                        })
                    })
                    setUsers(data)
                }
            }, {
                onlyOnce: false
            });

            const urlHistory = `messages/${res.uid}/`
            return onValue(ref(db, urlHistory), (snapshot) => {
                if (snapshot.val()) {
                    console.log('Data history: ', snapshot.val())
                    const oldData = snapshot.val()
                    const data = []
                    Object.keys(oldData).map(key => {
                        data.push({
                            id: key,
                            ...oldData[key],
                        })
                    })
                    setHistoryChat(data)
                }
            }, {
                onlyOnce: false
            });
        })
    }, [])

    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <View style={styles.navigationAdmin}>
                    <Text style={styles.title}>Messages</Text>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Text style={styles.homeButton}>Home</Text>
                    </TouchableOpacity>
                </View>
                {
                    users.map(user => {
                        if (user.data.role === 'user') {
                            return <List key={user.id} profile={{ uri: user.data.photo }} name={user.data.fullName} desc={
                                historyChat.map(chat => {
                                    if (user.data.uid === chat.uidPartner) {
                                        return chat.lastContentChat
                                    }
                                })
                            } onPress={() => navigation.navigate('ChattingAdmin', { user })} />
                        }
                    })
                }
            </View>
        </View>
    )
}

export default MessagesAdmin

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary,
        flex: 1,
    },
    content: {
        backgroundColor: colors.dark,
        flex: 1,
    },
    navigationAdmin: {
        flexDirection: 'row',
        marginRight: 16,
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginLeft: 16,
    },
    homeButton: {
        fontSize: 13,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 10,
        marginLeft: 16,
    }
})
