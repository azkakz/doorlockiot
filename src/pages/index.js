import Splash from "./Splash";
import GetStarted from "./GetStarted";
import Register from "./Register";
import Login from "./Login";
import UploadPhoto from "./UploadPhoto";
import Lock from "./Lock";
import Messages from "./Messages";
import Statistics from "./Statistics";
import Chatting from "./Chatting";
import UserProfile from "./UserProfile";
import UpdateProfile from "./UpdateProfile";
import AdminHome from "./AdminHome";
import MessagesAdmin from "./MessagesAdmin";
import ChattingAdmin from "./ChattingAdmin";
import StatisticsAdmin from "./StatisticsAdmin";

export { Splash, GetStarted, Register, Login, UploadPhoto, Lock, Messages, Statistics, Chatting, UserProfile, UpdateProfile, AdminHome, MessagesAdmin, ChattingAdmin, StatisticsAdmin }