import React, { useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ILLogo } from '../../assets'
import { colors, fonts, getData } from '../../utils';
import { Firebase } from '../../config'
import { getAuth, onAuthStateChanged } from "firebase/auth";

const Splash = ({ navigation }) => {
    useEffect(async () => {
        const auth = getAuth(Firebase);
        const userData = await getData('user')
        if (userData) {
            navigation.replace(userData.role === 'admin' ? 'AdminHome' : 'MainApp')
        } else {
            navigation.replace('GetStarted')
        }
    }, []);

    return (
        <View style={styles.page}>
            <ILLogo />
            <Text style={styles.title}>IoT DoorLock</Text>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.dark,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: '600',
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 20,
    }
})
