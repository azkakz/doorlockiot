import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { DummyUser } from '../../assets'
import { DataAllUser, Gap } from '../../components'
import { colors, fonts, showError, storeData } from '../../utils'
import { Firebase } from '../../config'
import { getAuth, signOut } from "firebase/auth";
import { getDatabase, ref, set, update, get, child, onValue } from "firebase/database";

const AdminHome = ({ navigation }) => {
    const [users, setUsers] = useState([])

    useEffect(() => {
        const db = getDatabase();
        return onValue(ref(db, `users/`), (snapshot) => {
            console.log('data all: ', snapshot.val())
            if (snapshot.val()) {
                const oldData = snapshot.val()
                const data = []
                Object.keys(oldData).map(key => {
                    data.push({
                        id: key,
                        data: oldData[key]
                    })
                })
                console.log('data hasil parse: ', oldData)
                setUsers(data)
            }
        }, {
            onlyOnce: false
        });

    }, [])

    const addremoveAccess = ({ user }) => {
        const dataUser = user
        const statusUser = {
            status: dataUser.data.status === false ? true : false
        }
        const db = getDatabase();
        console.log('ganti status', dataUser.id, statusUser)
        update(ref(db, 'users/' + dataUser.id + '/'), statusUser);
    }


    const signOutAccount = () => {
        const auth = getAuth(Firebase);
        signOut(auth).then(async () => {
            await storeData('user', null)
            navigation.replace('GetStarted')
        }).catch((error) => {
            // An error happened.
            showError(error.message)
        });
    }

    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <View style={styles.navigationAdmin}>
                    <Text style={styles.title}>Data All User</Text>
                    <TouchableOpacity onPress={signOutAccount}>
                        <Text style={styles.signOut}>Sign Out</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('MessagesAdmin')}>
                    <Text style={styles.signOut}>Messages</Text>
                </TouchableOpacity>
                <Gap height={15} />
                {
                    users.map(user => {
                        if (user.data.role === 'user') {
                            return <DataAllUser key={user.id} profile={{ uri: user.data.photo }} name={user.data.fullName} ruangan={user.data.ruangan} status={user.data.status} onPress={() => navigation.navigate('StatisticsAdmin', { user })} onPressAccess={() => addremoveAccess({ user })} />
                        }
                    })
                }
            </View>
        </View>
    )
}

export default AdminHome

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary,
        flex: 1,
    },
    content: {
        backgroundColor: colors.dark,
        flex: 1,
    },
    navigationAdmin: {
        flexDirection: 'row',
        marginRight: 16,
        justifyContent: 'space-between'
    },
    title: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginLeft: 16,
    },
    signOut: {
        fontSize: 13,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 10,
        marginLeft: 16,
    }
})