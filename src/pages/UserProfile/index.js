import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ILNullPhoto } from '../../assets'
import { Gap, Header, List, Profile } from '../../components'
import { colors, getData, showError, storeData } from '../../utils'
import { Firebase } from '../../config'
import { getAuth, signOut } from "firebase/auth";


const UserProfile = ({ navigation }) => {
    const [profile, setProfile] = useState({
        fullName: '',
        ruangan: '',
        photo: ILNullPhoto,
        status: '',
    })

    useEffect(() => {
        getData('user').then(res => {
            const data = res
            data.photo = { uri: res.photo }
            data.statusruangan = data.status ? 'Verified' : 'Not Verified'
            setProfile(data)
        })
    }, [])

    const signOutAccount = async () => {
        const auth = getAuth(Firebase);
        await storeData('user', null)
        signOut(auth).then(() => {
            // Sign-out successful.
            // const userData = getData('user')
            // console.log('hasil logout', userData)
            navigation.replace('GetStarted')
        }).catch((error) => {
            // An error happened.
            showError(error.message)
        });
    }

    return (
        <View style={styles.page}>
            <Header title="Profile" onPress={() => navigation.goBack()} />
            <Gap height={10} />
            {profile.fullName.length > 0 && (
                <Profile name={profile.fullName} desc={profile.ruangan} photo={profile.photo} status={profile.statusruangan} />
            )}
            <Gap height={14} />
            <List name="Edit Profile" desc="Last Update Yesterday" type="next" icon="edit-profile" /*onPress={() => navigation.navigate('UpdateProfile')}*/ />
            <List name="Language" desc="Available 12 languages" type="next" icon="language" />
            <List name="Give Us Rate" desc="On Google Play Store" type="next" icon="rate" />
            <List name="Sign Out" desc="Press to sign out" type="next" icon="signout" onPress={signOutAccount} />
        </View>
    )
}

export default UserProfile

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.dark,
    }
})
