import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { ILLogo } from '../../assets'
import { Button, Input, Link, Gap } from '../../components'
import { colors, fonts, showError, storeData, useForm } from '../../utils'
import { Firebase } from '../../config'
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { getDatabase, ref, child, get } from "firebase/database";
import { useDispatch } from 'react-redux'

const Login = ({ navigation }) => {
    const [form, setForm] = useForm({ email: '', password: '' })
    const dispatch = useDispatch()

    const login = () => {
        dispatch({ type: 'SET_LOADING', value: true })
        const auth = getAuth(Firebase);
        signInWithEmailAndPassword(auth, form.email, form.password)
            .then((res) => {
                // Signed in
                const user = res.user;
                dispatch({ type: 'SET_LOADING', value: false })
                const dbRef = ref(getDatabase());
                get(child(dbRef, `users/${res.user.uid}/`)).then((snapshot) => {
                    if (snapshot.exists()) {
                        storeData('user', snapshot.val())
                        const res = snapshot.val()
                        console.log('res di login', res)
                        navigation.replace(res.role === "admin" ? 'AdminHome' : 'MainApp')
                    } else {
                    }
                }).catch((error) => {
                    const errorMessage = error.message;
                    console.log(error)
                    showError(errorMessage)
                });
            })
            .catch((error) => {
                const errorMessage = error.message;
                dispatch({ type: 'SET_LOADING', value: false })
                showError(errorMessage)
                console.log(error)
            });
    }

    return (
        <View style={styles.page} >
            <ScrollView showsVerticalScrollIndicator={false}>
                <Gap height={40} />
                <ILLogo />
                <Text style={styles.title}>Masuk dan mulai konfigurasi</Text>
                <Input label="Email Adress" value={form.email} onChangeText={(value) => setForm('email', value)} />
                <Gap height={24} />
                <Input label="Password" value={form.password} onChangeText={(value) => setForm('password', value)} secureTextEntry />
                <Gap height={10} />
                <Link title="Forgot My Password" size={12} />
                <Gap height={40} />
                <Button title="Sign In" onPress={login} />
                <Gap height={30} />
                <Link title="Create New Account" size={16} align="center" onPress={() => navigation.navigate('Register')} />
            </ScrollView>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    page: {
        paddingHorizontal: 40,
        backgroundColor: colors.dark,
        flex: 1,
    },
    title: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 40,
        marginBottom: 40,
        maxWidth: 153,

    }
})
