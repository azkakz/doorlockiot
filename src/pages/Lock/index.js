import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Gap, HomeProfile, LockStatus } from '../../components'
import { colors, fonts, getData, storeData } from '../../utils'
import { Firebase } from '../../config'
import { getDatabase, ref, set, update, get, child, onValue, push } from "firebase/database";
import { showMessage } from 'react-native-flash-message'

const Lock = ({ navigation }) => {
    const [ruangan, setRuangan] = useState('')
    const [status, setStatus] = useState('')
    const [key, setKey] = useState()

    useEffect(() => {
        getData('user').then(res => {
            const data = res

            const db = getDatabase();
            onValue(ref(db, `users/${data.uid}/`), (snapshot) => {
                const datalive = (snapshot.val()) || 'Gagal memuat';
                setStatus(datalive.status)
                setRuangan(datalive.ruangan)
            }, {
                onlyOnce: false
            });

            return onValue(ref(db, `ruangan/${data.ruangan}/`), (snapshot) => {
                const key = (snapshot.val()) || 'Gagal';
                const keystatus = key.key
                setKey(keystatus === 0 ? 'Locked' : 'Unlocked')
            }, {
                onlyOnce: false
            });
        })
    }, [])

    const setLock = () => {
        if (status) {
            const keyLock = (key === 'Locked' ? 1 : 0)
            const db = getDatabase();
            update(ref(db, `ruangan/${ruangan}/`), { key: keyLock })
                .then(() => {
                })
                .catch((error) => {
                    const errorMessage = error.message;
                    showMessage({
                        message: errorMessage,
                        type: 'default',
                        backgroundColor: colors.error,
                        color: colors.white,
                    })
                })
            if (key === 'Locked') {
                const timenow = new Date().valueOf() / 1000 + 25200
                console.log('time click: ', timenow)
                push(ref(db, `ruangan/${ruangan}/waktuOpen`), timenow)
                    .then(() => {
                    })
                    .catch((error) => {
                    })
            }
        }
        else {
            showMessage({
                message: 'Akun anda belum terverifikasi',
                type: 'default',
                backgroundColor: colors.error,
                color: colors.white,
            })
        }
    }

    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <HomeProfile onPress={() => navigation.navigate('UserProfile')} />
                <Gap height={114} />
                <View style={styles.lockstatus}>
                    <LockStatus isLock={key} onPress={setLock} />
                    <Gap height={50} />
                    <Text style={styles.textlockstatus}>Status: <Text style={styles.statuskey(key)}>{key}</Text></Text>
                </View>
            </View>
        </View>
    )
}

export default Lock

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary,
        flex: 1,
    },
    content: {
        paddingVertical: 30,
        paddingHorizontal: 30,
        backgroundColor: colors.dark,
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    lockstatus: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textlockstatus: {
        fontSize: 18,
        fontFamily: fonts.primary[300],
        color: colors.text.primary,
    },
    statuskey: (key) => ({
        fontSize: 18,
        fontFamily: fonts.primary[300],
        color: key === 'Locked' ? colors.text.statopen : colors.text.statclose,
    })
})
