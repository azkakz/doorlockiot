import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Gap, ListStatistics } from '../../components'
import { colors, fonts, getData } from '../../utils'
import { Firebase } from '../../config'
import { getDatabase, ref, set, update, get, child, onValue } from "firebase/database";

const Statistics = () => {
    const [historyOpen, setHistoryOpen] = useState([])
    const [historyClose, setHistoryClose] = useState([])
    const [historyKey] = ([historyOpen.concat(historyClose)])
    historyKey.sort(function (a, b) { return a.timestamp - b.timestamp });

    useEffect(() => {
        getData('user').then(res => {
            const db = getDatabase();
            onValue(ref(db, `ruangan/${res.ruangan}/waktuOpen`), (snapshot) => {
                if (snapshot.val()) {
                    const oldData = snapshot.val()
                    const data = []
                    Object.keys(oldData).map(key => {
                        data.push({
                            id: key,
                            time: new Date(oldData[key] * 1000).toUTCString() + ' (Open)',
                            timestamp: oldData[key],
                            keterangan: 'buka'
                        })
                    })
                    setHistoryOpen(data)
                }
            }, {
                onlyOnce: false
            });

            onValue(ref(db, `ruangan/${res.ruangan}/waktuClose`), (snapshot) => {
                if (snapshot.val()) {
                    const oldData = snapshot.val()
                    const data = []
                    Object.keys(oldData).map(key => {
                        data.push({
                            id: key,
                            time: new Date(oldData[key] * 1000).toUTCString() + ' (Close)',
                            timestamp: oldData[key],
                            keterangan: 'tutup'
                        })
                    })
                    setHistoryClose(data)
                }
            }, {
                onlyOnce: false
            });
        })
    }, [])

    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <Text style={styles.title}>Statistics</Text>
                <Gap height={15} />
                {
                    historyKey.map(historykey => {
                        return <ListStatistics key={historykey.id} timeOpen={historykey.time} statcolor={historykey.keterangan} />
                    })
                }
            </View>
        </View>
    )
}

export default Statistics

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.secondary,
        flex: 1,
    },
    content: {
        backgroundColor: colors.dark,
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    title: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginLeft: 16,
    },
})
