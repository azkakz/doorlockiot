import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { ChatItem, Header, InputChat } from '../../components'
import { colors, fonts, getChatTime, getData, setDateChat, showError } from '../../utils'
import { Firebase } from '../../config'
import { getDatabase, ref, set, update, get, child, onValue, push } from "firebase/database";

const Chatting = ({ navigation, route }) => {
    const dataAdmin = route.params
    const [chatContent, setChatContent] = useState("")
    const [user, setUser] = useState({})
    const [chatData, setChatData] = useState([])

    useEffect(() => {
        getData('user').then(res => {
            const chatID = `${res.uid}_${dataAdmin.admin.data.uid}`
            const urlFirebase = `chatting/${chatID}/allChat/`
            const db = getDatabase();
            onValue(ref(db, urlFirebase), (snapshot) => {
                if (snapshot.val()) {
                    const dataSnapshot = snapshot.val()
                    const allDataChat = []
                    Object.keys(dataSnapshot).map(key => {
                        const dataChat = dataSnapshot[key]
                        const newDataChat = []
                        Object.keys(dataChat).map(itemChat => {
                            newDataChat.push({
                                id: itemChat,
                                data: dataChat[itemChat]
                            })
                        })
                        allDataChat.push({
                            id: key,
                            data: newDataChat
                        })
                    })
                    setChatData(allDataChat)
                }
            }, {
                onlyOnce: false
            });
            setUser(res)
        })
    }, [])

    const chatSend = () => {
        const today = new Date()
        const data = {
            sendBy: user.uid,
            chatDate: today.getTime(),
            chatTime: getChatTime(today),
            chatContent: chatContent,
        }
        const chatID = `${user.uid}_${dataAdmin.admin.data.uid}`
        const urlFirebase = `chatting/${chatID}/allChat/${setDateChat(today)}`
        const urlMessageUser = `messages/${user.uid}/${chatID}`
        const urlMessageAdmin = `messages/${dataAdmin.admin.data.uid}/${chatID}`
        const dataHistoryChatForUser = {
            lastContentChat: chatContent,
            lastChatDate: today.getTime(),
            uidPartner: dataAdmin.admin.data.uid,
        }
        const dataHistoryChatForAdmin = {
            lastContentChat: chatContent,
            lastChatDate: today.getTime(),
            uidPartner: user.uid,
        }
        setChatContent("")
        // kirim ke firebase
        const db = getDatabase()
        push(ref(db, urlFirebase), (data)).then(res => {
            setChatContent('')
            // set history untuk user
            const db = getDatabase()
            set(ref(db, urlMessageUser), (dataHistoryChatForUser))
            // set history untuk admin
            set(ref(db, urlMessageAdmin), (dataHistoryChatForAdmin))
        }).catch(err => {
            showError(err.message)
        })
    }

    return (
        <View style={styles.page}>
            <Header type="admin-profile" title={dataAdmin.admin.data.fullName} desc={dataAdmin.admin.data.ruangan} photo={{ uri: dataAdmin.admin.data.photo }} onPress={() => navigation.goBack()} />
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {
                        chatData.map(chat => {
                            return (
                                <View key={chat.id}>
                                    <Text style={styles.chatDate}>{chat.id}</Text>
                                    {chat.data.map(itemChat => {
                                        const isMe = itemChat.data.sendBy === user.uid
                                        return <ChatItem key={itemChat.id} isMe={isMe} text={itemChat.data.chatContent} date={itemChat.data.chatTime} photo={isMe ? null : { uri: dataAdmin.admin.data.photo }} />
                                    })}
                                </View>
                            )
                        })
                    }
                </ScrollView>
            </View>
            <InputChat value={chatContent} onChangeText={value => setChatContent(value)} onButtonPress={chatSend} />
        </View>
    )
}

export default Chatting

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.dark,
        flex: 1,
    },
    content: {
        flex: 1,
    },
    chatDate: {
        fontSize: 11,
        fontFamily: fonts.primary.normal,
        color: colors.text.secondary,
        marginVertical: 20,
        textAlign: 'center',
    }
})
