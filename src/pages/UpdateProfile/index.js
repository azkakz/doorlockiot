import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { Button, Gap, Header, Input, Profile } from '../../components'
import { colors, getData, storeData } from '../../utils'
import { Firebase } from '../../config'
import { getDatabase, ref, set, update } from "firebase/database";
import { getAuth, updatePassword } from "firebase/auth";
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { ILNullPhoto } from '../../assets'
import { showMessage } from 'react-native-flash-message'

const UpdateProfile = ({ navigation }) => {
    const [profile, setProfile] = useState({
        fullName: '',
        ruangan: '',
        email: '',
    })
    const [password, setPassword] = useState('')
    const [photo, setPhoto] = useState(ILNullPhoto)
    const [photoForDB, setPhotoForDB] = useState('')

    useEffect(() => {
        getData('user').then(res => {
            const data = res
            setPhoto({ uri: res.photo })
            setProfile(data)
        })
    }, [])

    const updateData = () => {
        console.log('profile: ', profile)
        console.log('new password: ', password)

        if (password.length > 0) {
            if (password.length < 6) {
                showMessage({
                    message: 'Password kurang dari 6 karakter',
                    type: 'default',
                    backgroundColor: colors.error,
                    color: 'white',
                })
            } else {
                updatePasswordData()
                updateProfileData()
                navigation.replace('MainApp')
            }
        } else {
            updateProfileData()
            navigation.replace('MainApp')
        }
    }

    const updateProfileData = () => {
        const data = profile
        data.photo = photoForDB
        const db = getDatabase();
        update(ref(db, `users/${profile.uid}/`), data)
            .then(() => {
                console.log('success: ', data)
                storeData('user', data)
            })
            .catch((error) => {
                const errorMessage = error.message;
                showMessage({
                    message: errorMessage,
                    type: 'default',
                    backgroundColor: colors.error,
                    color: colors.white,
                })
            })
    }

    const updatePasswordData = () => {
        const auth = getAuth(Firebase);

        const user = auth.currentUser;

        updatePassword(user, password).then(() => {
            // Update successful.
            console.log('success update password')

        }).catch((error) => {
            showMessage({
                message: error,
                type: 'default',
                backgroundColor: colors.error,
                color: 'white',
            })
        });
    }

    const changeText = (key, value) => {
        setProfile({
            ...profile,
            [key]: value,
        })
    }

    const getImage = () => {
        launchImageLibrary({ includeBase64: true, quality: 0.5, maxHeight: 200, maxWidth: 200 }, response => {
            console.log('response: ', response)
            if (response.didCancel || response.error) {
                showMessage({
                    message: 'Oops, sepertinya anda tidak memilih fotonya?',
                    type: 'default',
                    backgroundColor: colors.error,
                    color: colors.white,
                })
            }
            else {
                // console.log('response getImage', response)
                setPhotoForDB(`data:${response.assets[0].type};base64, ${response.assets[0].base64}`)
                // console.log('response getImage', `data:${response.assets[0].type};base64, ${response.assets[0].base64}`)
                const source = { uri: response.assets[0].uri }
                setPhoto(source)
            }
        })
    }

    return (
        <View style={styles.page}>
            <Header title="Edit Profile" onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.content}>
                    <Profile isRemove photo={photo} onPress={getImage} />
                    <Gap height={26} />
                    <Input label="Full Name" value={profile.fullName} onChangeText={(value) => changeText('fullName', value)} />
                    <Gap height={24} />
                    <Input label="Ruangan" value={profile.ruangan} onChangeText={(value) => changeText('ruangan', value)} />
                    <Gap height={24} />
                    <Input label="Email" value={profile.email} disable />
                    <Gap height={24} />
                    <Input label="Password" value={password} onChangeText={(value) => setPassword(value)} secureTextEntry />
                    <Gap height={40} />
                    <Button title="Save Profile" onPress={updateData} />
                </View>
            </ScrollView>
        </View>
    )
}

export default UpdateProfile

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.dark,
        flex: 1,
    },
    content: {
        padding: 40,
        paddingTop: 0,
    }
})
