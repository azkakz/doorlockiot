import React, { useState, useEffect, useRef } from 'react'
import { ScrollView, StyleSheet, Text, View, Picker } from 'react-native'
import { Button, Gap, Header, Input, Loading } from '../../components'
import { colors, fonts, getData, showError, storeData, useForm } from '../../utils'
import { Firebase } from '../../config'
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { getDatabase, ref, set, onValue } from "firebase/database";
import { useDispatch } from 'react-redux'

const Register = ({ navigation }) => {
    const [form, setForm] = useForm({
        fullName: '',
        ruangan: '',
        email: '',
        password: '',
        role: 'user',
        status: false,
    })
    const [ruangans, setRuangans] = useState([])
    const dispatch = useDispatch()
    const pickerRef = useRef();

    useEffect(() => {
        const db = getDatabase();
        return onValue(ref(db, `ruangan/`), (snapshot) => {
            console.log('ruangan yang ada: ', snapshot.val())
            if (snapshot.val()) {
                const oldData = snapshot.val()
                const data = []
                Object.keys(oldData).map(key => {
                    data.push({
                        id: key,
                        data: oldData[key]
                    })
                })
                console.log('ruangan yang ada: ', data)
                setRuangans(data)
            }
        }, {
            onlyOnce: false
        });
    }, [])

    const onContinue = () => {
        const auth = getAuth(Firebase);
        dispatch({ type: 'SET_LOADING', value: true })
        createUserWithEmailAndPassword(auth, form.email, form.password)
            .then((success) => {
                dispatch({ type: 'SET_LOADING', value: false })
                setForm('reset')
                const data = {
                    fullName: form.fullName,
                    ruangan: form.ruangan,
                    email: form.email,
                    uid: success.user.uid,
                    role: form.role,
                    status: form.status,
                }
                const db = getDatabase();
                set(ref(db, 'users/' + success.user.uid + '/'), data);

                storeData('user', data)
                navigation.navigate('UploadPhoto', data)
            })
            .catch((error) => {
                const errorMessage = error.message;
                dispatch({ type: 'SET_LOADING', value: false })
                showError(errorMessage)
            });
    }

    return (
        <View style={styles.page}>
            <Header onPress={() => navigation.goBack()} title="Daftar Akun" />
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Input label="Full Name" value={form.fullName} onChangeText={(value) => setForm('fullName', value)} />
                    <Gap height={24} />
                    {/* <Input label="Ruangan" value={form.ruangan} onChangeText={(value) => setForm('ruangan', value)} /> */}
                    <View>
                        <Text style={styles.label}>Ruangan</Text>
                        <View style={styles.wrapperdropdown}>
                            <Picker ref={pickerRef} selectedValue={form} onValueChange={(value) => setForm('ruangan', value)} style={styles.dropdown}>
                                <Picker.Item label="Pilih" value="disabled" />
                                {
                                    ruangans.map(ruangan => {
                                        return <Picker.Item key={ruangan.id} label={ruangan.id} value={ruangan.id} />
                                    })
                                }
                            </Picker>
                        </View>
                    </View>
                    <Gap height={24} />
                    <Input label="Email" value={form.email} onChangeText={(value) => setForm('email', value)} />
                    <Gap height={24} />
                    <Input label="Password" value={form.password} onChangeText={(value) => setForm('password', value)} secureTextEntry />
                    <Gap height={40} />
                    <Button title="Continue" onPress={onContinue} />
                </ScrollView>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.dark,
        flex: 1,
    },
    content: {
        padding: 40,
        paddingTop: 0,
    },
    label: {
        fontSize: 16,
        color: colors.text.primary,
        marginBottom: 6,
        fontFamily: fonts.primary[400],
    },
    wrapperdropdown: {
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius: 10,
        color: colors.text.primary,
    },
    dropdown: {
        color: colors.text.primary,
    }
})
